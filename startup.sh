#!/bin/sh

echo "Activating service account..."
python3 generate_key.py key.json
gcloud auth activate-service-account --key-file key.json
echo "...complete"


if [ -z "$PROJECT_ID" ]
then
echo "setting project = $PROJECT_ID"
gcloud config set project $PROJECT_ID
fi

if [ -z "$COMPUTE_ZONE" ]
then
echo "setting compute/zone = $COMPUTE_ZONE"
gcloud config set compute/zone $COMPUTE_ZONE
fi

if [ -z "$CLUSTER_NAME" ]
then
echo "setting container credentials for cluster = $CLUSTER_NAME"
gcloud container clusters get-credentials $CLUSTER_NAME
fi

echo "cleaning up"

rm key.json
rm generate_key.py

echo "Initialization complete!"
