FROM google/cloud-sdk:latest
RUN yes | apt-get install python3

COPY startup.sh /startup.sh
COPY generate_key.py /generate_key.py

ENTRYPOINT ["/startup.sh","&&","sh"]

