Example usage:

```
pipeline:
  # if you want to push an image to google container registry just use standard means
  gcr:
    image: plugins/gcr
    registry: eu.gcr.io
    repo: codementor-tutorial/codementor-tutorial
    tags: ["commit_${DRONE_COMMIT}","build_${DRONE_BUILD_NUMBER}", "latest"]
    secrets: [GOOGLE_CREDENTIALS]
    when:
      branch: master

  deploy:
    image: zaprelude/cloud-sdk:latest
    environment:
      PROJECT_ID: codementor-tutorial # not required
      COMPUTE_ZONE: europe-west1-d    # not required
      CLUSTER_NAME: hello-codementor  # not required
    secrets: [GOOGLE_CREDENTIALS] # just copy paste your credentials key json text into drone's secret gui. dont base64 encode it
    commands:
      - kubectl set image deployment/codementor-tutorial codementor-tutorial=eu.gcr.io/$${PROJECT_ID}/codementor-tutorial:commit_${DRONE_COMMIT}
    when:
      branch: master
```
